package juego;

import java.awt.Image;

import entorno.Herramientas;
import entorno.Entorno;

public class Nave {

	private double x;
	private double y;

	private double angulo;
	private double size;
	private double velocidad;
	private Image img;

	public Nave(double x, double y, double angulo, double size, double velocidad) {
		this.x = x;
		this.y = y;
		this.angulo = angulo;
		this.size = size;
		this.velocidad = velocidad;
		this.img = Herramientas.cargarImagen("nave.png");
	}

	public void dibujar(Entorno e) {
		e.dibujarImagen(img, x, y, angulo, size);
	}

	public void moverDerecha(Entorno e) {
		this.x += velocidad;

		if (this.x > e.ancho() * 0.95) {
			this.x = e.ancho() * 0.95;
		}
	}

	public void moverIzquierda(Entorno e) {
		this.x -= velocidad;

		if (this.x < e.ancho() * 0.05) {
			this.x = e.ancho() * 0.05;
		}

	}

	public Proyectil disparar(Entorno e) {
		return new Proyectil(x, y, Math.PI * 3.0 / 2, 0.02, 20);
	}

	public boolean impactasteConEnemigo(Enemigo enemigo) {

		double xEnemigo = enemigo.getX();
		double yEnemigo = enemigo.getY();

		return ((this.x - xEnemigo) * (this.x - xEnemigo) + (this.y - yEnemigo) * (this.y - yEnemigo) < 1000);
	}

	public boolean impactasteConAsteroide(Asteroide asteroide) {

		double xAsteroide = asteroide.getX();
		double yAsteroide = asteroide.getY();

		return ((this.x - xAsteroide) * (this.x - xAsteroide) + (this.y - yAsteroide) * (this.y - yAsteroide) < 1000);
	}

	public boolean teDispararon(Proyectil proyectilEnemigo) {

		double xProyectilEnemigo = proyectilEnemigo.getX();
		double yProyectilEnemigo = proyectilEnemigo.getY();

		return ((this.x - xProyectilEnemigo) * (this.x - xProyectilEnemigo)
				+ (this.y - yProyectilEnemigo) * (this.y - yProyectilEnemigo) < 1000);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

}
