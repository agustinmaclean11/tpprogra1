package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Proyectil {

	private double x;
	private double y;
	private double angulo;
	private double size;
	private double velocidad;
	private Image img;

	public Proyectil(double x, double y, double angulo, double size, double velocidad) { 
		this.x = x;
		this.y = y;
		this.angulo = angulo;
		this.size = size;
		this.velocidad = velocidad;
		this.img = Herramientas.cargarImagen("bala1.png");
	}

	public void dibujar(Entorno e) {
		e.dibujarImagen(img, x, y, angulo, size);
	}

	public void avanzar() {
		this.y += velocidad * Math.sin(angulo);
	}

	public boolean estaDentro(Entorno e) {
		return !(this.x < -10 || this.x > e.ancho() || this.y < 0 || this.y > e.alto() + 200);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

}
