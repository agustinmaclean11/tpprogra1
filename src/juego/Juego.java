package juego;

import java.awt.Color;
import java.awt.Image;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	private Entorno entorno;

	private Image fondo;
	private Image fondoDeGameOver;
	private Image fondoDeWin;

	private Nave nave;
	private Proyectil proyectil;
	private Proyectil[] proyectilesEnemigos; 
	private Enemigo[] enemigosNivel1;
	private Enemigo[] enemigosNivel2;
	private Enemigo[] enemigosNivel3;
	private Asteroide[] asteroidesDerechos;
	private Asteroide[] asteroidesIzquierdos;
	private double eliminados;
	private Random aparicion = new Random();

	public Juego() {

		this.eliminados = 0;

		this.entorno = new Entorno(this, "Juego de Naves", 800, 600);

		this.fondo = Herramientas.cargarImagen("fondo.jpg");

		this.fondoDeGameOver = Herramientas.cargarImagen("gameover.png");

		this.fondoDeWin = Herramientas.cargarImagen("win.png");

		this.nave = new Nave(entorno.ancho() / 2, entorno.alto() * 0.90, Math.PI, 0.1, 4);

		this.enemigosNivel1 = new Enemigo[6];

		for (int i = 0; i < enemigosNivel1.length; i++) {
			this.enemigosNivel1[i] = new Enemigo(Math.random() * (entorno.ancho() - 100), aparicion.nextInt(300) * -1,
					Math.PI / 2, 0.25, 1, 2);
		}

		this.enemigosNivel2 = new Enemigo[6];

		for (int i = 0; i < enemigosNivel2.length; i++) {
			this.enemigosNivel2[i] = new Enemigo(Math.random() * (entorno.ancho() - 100), aparicion.nextInt(300) * -1,
					Math.PI / 2, 0.13, 2, 2);
		}

		this.enemigosNivel3 = new Enemigo[4];

		for (int i = 0; i < enemigosNivel3.length; i++) {
			this.enemigosNivel3[i] = new Enemigo(Math.random() * (entorno.ancho() - 100), aparicion.nextInt(300) * -1,
					Math.PI / 2, 0.1, 3, 2);
		}

		this.proyectilesEnemigos = new Proyectil[6];

		for (int i = 0; i < proyectilesEnemigos.length; i++) {
			this.proyectilesEnemigos[i] = enemigosNivel1[i].disparar(entorno); // pero no va aca, fixme
		}

		this.asteroidesDerechos = new Asteroide[4];

		for (int i = 0; i < asteroidesDerechos.length; i++) {
			this.asteroidesDerechos[i] = new Asteroide(Math.random() * (entorno.ancho() + 300),
					aparicion.nextInt(300) * -1, Math.PI * 3 / 4, 0.02, 1.5);
		}

		this.asteroidesIzquierdos = new Asteroide[4];

		for (int i = 0; i < asteroidesIzquierdos.length; i++) {
			this.asteroidesIzquierdos[i] = new Asteroide(Math.random() * (entorno.ancho() + 300),
					aparicion.nextInt(300) * -1, Math.PI / 3, 0.02, 1.5);
		}

		this.entorno.iniciar();
	}

	public void tick() {
		entorno.dibujarImagen(fondo, entorno.ancho() / 2, entorno.alto() / 2, 0);
		
		if (nave == null) {
			entorno.dibujarImagen(fondoDeGameOver, entorno.ancho() / 2, entorno.alto() / 2, 0, 3.5);
			entorno.cambiarFont("Impact", 20, Color.CYAN);
			entorno.escribirTexto("Presiona q para salir", 625, 590);
			if (entorno.estaPresionada('q')) {
				System.exit(0);
			}
			return;
		}
		
		if (eliminados == 16) {
			entorno.dibujarImagen(fondoDeWin, entorno.ancho() / 2, entorno.alto() / 2, 0, 4);
			entorno.cambiarFont("Impact", 20, Color.CYAN);
			entorno.escribirTexto("Presiona q para salir", 625, 590);
			if(entorno.estaPresionada('q')) {
				System.exit(0);
			}
			return;
		}
		
		if (nave != null) {
			nave.dibujar(entorno);
		}
		
		if (nave != null && entorno.estaPresionada('d')) {
			nave.moverDerecha(entorno);
		}

		if (nave != null && entorno.estaPresionada('a')) {
			nave.moverIzquierda(entorno);
		}

		if (nave != null && entorno.estaPresionada(entorno.TECLA_ESPACIO) && proyectil == null) {
			this.proyectil = nave.disparar(entorno);
		}

		if (proyectil != null) {
			proyectil.dibujar(entorno);
			proyectil.avanzar();

			if (!proyectil.estaDentro(entorno)) {
				proyectil = null;
			}
		}

		for (int i = 0; i < asteroidesDerechos.length; i++) {
			if (asteroidesDerechos[i] != null) {
				asteroidesDerechos[i].dibujar(entorno);
				asteroidesDerechos[i].avanzar();

				if (!asteroidesDerechos[i].estaDentro(entorno)) {
					asteroidesDerechos[i] = null;
				}
			}
			for (int j = i + 1; j < asteroidesDerechos.length; j++) {
				if (asteroidesDerechos[i] != null && asteroidesDerechos[j] != null && asteroidesDerechos[i].impactasteCon(asteroidesDerechos[j])) {
					asteroidesDerechos[i] = null;
					asteroidesDerechos[i] = new Asteroide(Math.random() * (entorno.ancho() + 300), Math.random() * (entorno.alto() - 800), Math.PI * 3 / 4, 0.02, 1.5);
				}
			}
			if (asteroidesIzquierdos[i] != null) {
				asteroidesIzquierdos[i].dibujar(entorno);
				asteroidesIzquierdos[i].avanzar();

				if (!asteroidesIzquierdos[i].estaDentro(entorno)) {
					asteroidesIzquierdos[i] = null;
				}
			}
			for (int j = i + 1; j < asteroidesIzquierdos.length; j++) {
				if (asteroidesIzquierdos[i] != null && asteroidesIzquierdos[j] != null && asteroidesIzquierdos[i].impactasteCon(asteroidesIzquierdos[j])) {
					asteroidesIzquierdos[i] = null;
					asteroidesIzquierdos[i] = new Asteroide(Math.random() * (entorno.ancho() + 300), Math.random() * (entorno.alto() - 800), Math.PI / 4, 0.02, 1.5);
				}
			}

		}

		for (int i = 0; i < enemigosNivel1.length; i++) {

			if (enemigosNivel1[i] != null) {
				enemigosNivel1[i].dibujar(entorno);
				enemigosNivel1[i].avanzar();
				
				if (!enemigosNivel1[i].estaDentroDePantalla(entorno)) {
					enemigosNivel1[i] = null;
					enemigosNivel1[i]= new Enemigo(Math.random()* entorno.ancho(),
							aparicion.nextInt(300) * -1, Math.PI / 2, 0.25,1 ,2);
				}
					
				for (int j = i + 1; j < enemigosNivel1.length; j++) {
					if (enemigosNivel1[i] != null &&  enemigosNivel1[j] != null && enemigosNivel1[i].impactasteConAliado(enemigosNivel1[j])) {
						enemigosNivel1[i] = null;
						enemigosNivel1[i] = new Enemigo(Math.random() * entorno.ancho(),
								aparicion.nextInt(300) * -1, Math.PI / 2, 0.25, 1, 2);
					}
				}

			}
		}
			if (eliminados >= 6 && eliminados <= 14) {
				
				for (int i = 0; i < enemigosNivel1.length; i++) {
					
				if (enemigosNivel2[i] != null) {
					enemigosNivel2[i].dibujar(entorno);
					enemigosNivel2[i].avanzar();
					enemigosNivel2[i].zigzag();
					
					if (!enemigosNivel2[i].estaDentroDePantalla(entorno)) {
						enemigosNivel2[i] = null;
						enemigosNivel2[i] = new Enemigo(Math.random() * entorno.ancho(),
								aparicion.nextInt(300) * -1, Math.PI / 2, 0.13, 2, 2);
					}
					for (int j = i + 1; j < enemigosNivel2.length; j++) {
						if (enemigosNivel2[i] != null && enemigosNivel2[j] != null && enemigosNivel2[i].impactasteConAliado(enemigosNivel2[j])) {
							enemigosNivel2[i] = null;
							enemigosNivel2[i] = new Enemigo(Math.random() * entorno.ancho(),
									aparicion.nextInt(300) * -1, Math.PI / 2, 0.13, 2, 2);
							}
						}

					}
				}
			}

		for (int i = 0; i < proyectilesEnemigos.length; i++) {

			if (proyectilesEnemigos[i] != null) {
				proyectilesEnemigos[i].dibujar(entorno);
				proyectilesEnemigos[i].avanzar(); 

				if (!proyectilesEnemigos[i].estaDentro(entorno)) {
					proyectilesEnemigos[i] = null;
				}
			}

			if (enemigosNivel1[i] != null && proyectilesEnemigos[i] == null && enemigosNivel1[i].estaDentroDePantalla(entorno)) {
				proyectilesEnemigos[i] = enemigosNivel1[i].disparar(entorno);
			}
			if (proyectilesEnemigos[i] != null) {
				proyectilesEnemigos[i].dibujar(entorno);
				proyectilesEnemigos[i].avanzar();

				if (!proyectilesEnemigos[i].estaDentro(entorno)) {
					proyectilesEnemigos[i] = null;
				}
			}

			if (enemigosNivel2[i] != null && proyectilesEnemigos[i] == null && enemigosNivel2[i].estaDentroDePantalla(entorno)) {
				proyectilesEnemigos[i] = enemigosNivel2[i].disparar(entorno);
			}
		}
	

		if (eliminados >= 12 && eliminados <= 16) {
			for (int i = 0; i < enemigosNivel3.length; i++) {
				if (nave != null && enemigosNivel3[i] != null) {
					enemigosNivel3[i].perseguir(nave, entorno);
				}
				if (enemigosNivel3[i] != null) {
					enemigosNivel3[i].dibujar(entorno);
					enemigosNivel3[i].avanzar();

					if (!enemigosNivel3[i].estaDentroDePantalla(entorno)) {
						enemigosNivel3[i] = null;
						enemigosNivel3[i] = new Enemigo(Math.random() * entorno.ancho(),
								aparicion.nextInt(300) * -1, Math.PI / 2, 0.1, 3, 2);
					}
				}

			}
		}

		for (int i = 0; i < proyectilesEnemigos.length - 2; i++) {

			if (proyectilesEnemigos[i] != null) {
				proyectilesEnemigos[i].dibujar(entorno);
				proyectilesEnemigos[i].avanzar();

				if (!proyectilesEnemigos[i].estaDentro(entorno)) {
					proyectilesEnemigos[i] = null;
				}
			}

			if (enemigosNivel3[i] != null && proyectilesEnemigos[i] == null && enemigosNivel3[i].estaDentroDePantalla(entorno)) {
				proyectilesEnemigos[i] = enemigosNivel3[i].disparar(entorno);
			}

		}
//impactos
		for (int i = 0; i < asteroidesDerechos.length; i++) {
			if (proyectil != null && asteroidesDerechos[i] != null && asteroidesDerechos[i].teDispararon(proyectil)) {
				asteroidesDerechos[i] = null;
				proyectil = null;
			}
			if (proyectil != null && asteroidesIzquierdos[i] != null && asteroidesIzquierdos[i].teDispararon(proyectil)) {
				asteroidesIzquierdos[i] = null;
				proyectil = null;
			}
			if (nave != null && asteroidesDerechos[i] != null && nave.impactasteConAsteroide(asteroidesDerechos[i])) {
				asteroidesDerechos[i] = null;
				nave = null;
			}
			if (nave != null && asteroidesIzquierdos[i] != null && nave.impactasteConAsteroide(asteroidesIzquierdos[i])) {
				asteroidesIzquierdos[i] = null;
				nave = null;
			}
		}

		for (int i = 0; i < enemigosNivel1.length; i++) {

			if (enemigosNivel1[i] != null && proyectil != null && enemigosNivel1[i].teDispararon(proyectil)) {
				enemigosNivel1[i] = null;
				proyectil = null;
				eliminados++;
			}
			
			if (enemigosNivel1[i] != null && nave != null && nave.impactasteConEnemigo(enemigosNivel1[i])){
				nave = null;
				enemigosNivel1[i] = null;
			}

			if (enemigosNivel2[i] != null && proyectil != null && enemigosNivel2[i].teDispararon(proyectil)) {
				enemigosNivel2[i] = null;
				proyectil = null;
				eliminados++;
			}
			if (enemigosNivel2[i] != null && nave != null && nave.impactasteConEnemigo(enemigosNivel2[i])) {
				nave = null;
				enemigosNivel2[i] = null;
			}

		}

		for (int i = 0; i < enemigosNivel3.length; i++) {

			if (enemigosNivel3[i] != null && nave != null && nave.impactasteConEnemigo(enemigosNivel3[i])) {
				nave = null;
				enemigosNivel3[i] = null;
			}
			if (enemigosNivel3[i] != null && proyectil != null && enemigosNivel3[i].teDispararon(proyectil)) {
				enemigosNivel3[i] = null;
				proyectil = null;
				eliminados++;
			}
			
		}

		for (int i = 0; i < proyectilesEnemigos.length; i++) {

			if (proyectilesEnemigos[i] != null && nave != null && nave.teDispararon(proyectilesEnemigos[i])) {
				nave = null;
				proyectilesEnemigos[i] = null;
			}
		}

		if (eliminados < 6) {
			entorno.cambiarFont("Impact", 20, Color.MAGENTA);
			entorno.escribirTexto("Oleada: 1", 10, 20);
		}
		if (eliminados < 12 && eliminados > 5) {
			entorno.cambiarFont("Impact", 20, Color.ORANGE);
			entorno.escribirTexto("Oleada: 2", 10, 20);

		}

		if (eliminados > 11 && eliminados <= 16) {
			entorno.cambiarFont("Impact", 20, Color.BLUE);
			entorno.escribirTexto("Oleada: 3", 10, 20);

		}

		

		
		
		entorno.cambiarFont("Impact", 20, Color.CYAN);
		entorno.escribirTexto("Eliminados: " + eliminados, 10, 590);
		
		}
	

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
