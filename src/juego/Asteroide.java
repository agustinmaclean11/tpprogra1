package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Asteroide {

	private double x;
	private double y;
	private double angulo;
	private double size;
	private double velocidad;
	private Image img;

	public Asteroide(double x, double y, double angulo, double size, double velocidad) {
		this.x = x;
		this.y = y;
		this.angulo = angulo;
		this.size = size;
		this.velocidad = velocidad;
		this.img = Herramientas.cargarImagen("asteroide.png");
	}

	public void dibujar(Entorno e) {
		e.dibujarImagen(img, x, y, angulo, size);
	}

	public void avanzar() {
		this.x += velocidad * Math.cos(angulo);
		this.y += velocidad * Math.sin(angulo);
	}

	public boolean estaDentro(Entorno e) {
		return !(this.x < -400 || this.x > e.ancho() + 400 || this.y < -400 || this.y > e.alto() + 100);
	}

	public boolean impactasteCon(Asteroide asteroide) {
		double xE = asteroide.getX();
		double yE = asteroide.getY();
		return ((this.x - xE) * (this.x - xE) + (this.y - yE) * (this.y - yE) < 1000);
	}

	public boolean teDispararon(Proyectil proyectil) {
		double xProyectil = proyectil.getX();
		double yProyectil = proyectil.getY();

		return ((this.x - xProyectil) * (this.x - xProyectil) + (this.y - yProyectil) * (this.y - yProyectil) < 1000);

	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
}
