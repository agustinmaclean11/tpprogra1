package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Enemigo {

	private double x;
	private double y;

	private double angulo;
	private double size;
	private double velocidad;
	private Image img;

	public Enemigo(double x, double y, double angulo, double size, int tipoDeImagen, double velocidad) {
		this.x = x;
		this.y = y;
		this.angulo = angulo;
		this.size = size;
		this.velocidad = velocidad;
		if (tipoDeImagen == 1) {
			this.img = Herramientas.cargarImagen("enemigo.png");
		}

		if (tipoDeImagen == 2) {
			this.img = Herramientas.cargarImagen("enemigo2.png");
		}

		if (tipoDeImagen == 3) {
			this.img = Herramientas.cargarImagen("nave.png");
		}

	}

	public void dibujar(Entorno e) {
		e.dibujarImagen(img, x, y, 0, size);
	}

	public void avanzar() {
		this.x += velocidad * Math.cos(angulo);
		this.y += velocidad * Math.sin(angulo);

	}

	public boolean estaDentroDePantalla(Entorno e) {
		return !(this.x < 15 || this.x > e.ancho() - 50 || this.y < -500 || this.y > e.alto());
	}

	public void zigzag() { // por qué 800? entorno.ancho?
		if ((this.y >= 60 && this.x > 30) || ((this.x < 800) && (this.x > 720))) {
			this.angulo = Math.PI * 3 / 4;
		}
		if (this.y > 200 || ((this.x > 0)) && (this.x < 20)) {
			this.angulo = Math.PI / 4;
		}
		if ((this.y > 300 && this.x > 30) || ((this.x < 800) && (this.x > 720))) {
			this.angulo = Math.PI * 3 / 4;
		}
		if ((this.y > 400 || ((this.x > 0)) && (this.x < 20))) {
			this.angulo = Math.PI / 4;
		}
		if ((this.y > 500 && this.x > 30) || ((this.x < 800) && (this.x > 720))) {
			this.angulo = Math.PI * 3 / 4;
		}

	}

	public void perseguir(Nave nave, Entorno e) {

		double xN = nave.getX();
		double yN = nave.getY();

		if (this.y > e.alto() * 0.30 && this.y < e.alto() * 0.70) {
			this.angulo = Math.atan2((yN - this.y), (xN - this.x));
			this.velocidad = 4;
		}
		if (this.y > e.alto() * 0.71) {
			this.angulo = Math.PI / 2;
		}
	}

	public Proyectil disparar(Entorno e) {
		return new Proyectil(x, y, Math.PI / 2, 0.02, 2);
	}

	public boolean teDispararon(Proyectil proyectil) {

		double xProyectil = proyectil.getX();
		double yProyectil = proyectil.getY();

		return ((x - xProyectil) * (x - xProyectil) + (y - yProyectil) * (y - yProyectil) < 1000);

	}

	public boolean impactasteConAliado(Enemigo aliado) {

		double xAliado = aliado.getX();
		double yAliado = aliado.getY();

		return ((x - xAliado) * (x - xAliado) + (y - yAliado) * (y - yAliado) < 1000);

	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

}
